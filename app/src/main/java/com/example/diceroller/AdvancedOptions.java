package com.example.diceroller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdvancedOptions extends Activity implements View.OnClickListener {

    RelativeLayout options;       //holds all the dice selection options
    LinearLayout optionOne;     //first dice selection option
    Button addDice;             //tracks button that adds more dice options
    Button removeDice;          //first removeDice button
    Button rollbtn;             //rolls dice
    Button backbtn;             //back a page

    int currentID;
    int removeID;

    ArrayList<Button> removeButtonsList;    //List of buttons to remove dice options offset by 1
    ArrayList<LinearLayout> newDiceOptions; //List of all the option selctors
    ArrayList<EditText> amountOfDiceList;   //list of edit text fields that select amount of dice
    ArrayList<EditText> sideSelectorList;   //list of edit text fields that select amount of sides

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advanced_options);

        currentID = 10000;
        removeID = 20000;

        options = findViewById(R.id.options);

        optionOne = findViewById(R.id.options_one);

        addDice = findViewById(R.id.addDice);
        addDice.setOnClickListener(this);

        removeDice = findViewById(R.id.remove_option);
        removeDice.setOnClickListener(this);

        //roll dice
        rollbtn = findViewById(R.id.a_o_roll);
        rollbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rollDice();
            }
        });

        //go back a screen
        backbtn = findViewById(R.id.a_o_back);
        backbtn.setOnClickListener(this);

        removeButtonsList = new ArrayList<>();
        newDiceOptions = new ArrayList<>();
        amountOfDiceList = new ArrayList<>();
        sideSelectorList = new ArrayList<>();

        removeButtonsList.add(removeDice);
        newDiceOptions.add(optionOne);  //add default option one
        amountOfDiceList.add((EditText) findViewById(R.id.option_one_amount));   //add first amount selector
        sideSelectorList.add((EditText) findViewById(R.id.option_one_side));     //add first side selector
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == addDice.getId()) {
            addNewDice();
        } else if (view.getId() == backbtn.getId()) {
            goToMultiDice();
        } else {
            removeDiceOption((Button) findViewById(view.getId()));
        }
    }

    public void goToMultiDice(){
        Intent goToMD = new Intent(this, MultiDice.class);
        startActivity(goToMD);
    }


    public void rollDice(){
        CustomRoll customRoll = new CustomRoll(newDiceOptions.size());

        for(int i = 0; i < newDiceOptions.size(); i++){
            EditText amountOfDice = amountOfDiceList.get(i);
            EditText numberOfSides = sideSelectorList.get(i);

            if (TextUtils.isEmpty(amountOfDice.getText())
                    || TextUtils.isEmpty((numberOfSides.getText()))) {
                Toast t = Toast.makeText
                        (this, "Fields Empty", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                return;
            }

            customRoll.dice[i].setNumberOfDice(Integer.parseInt(amountOfDice.getText().toString()));
            customRoll.dice[i].setNumberOfSide(Integer.parseInt(numberOfSides.getText().toString()));
        }

        Intent goToCustomRoll = new Intent(this, RollCustomDice.class);
        goToCustomRoll.putExtra("Custom roll", customRoll);
        startActivity(goToCustomRoll);
    }

    public void removeDiceOption(Button removeOption) {
        int index = removeButtonsList.indexOf(removeOption);

        if(index == 0 && newDiceOptions.size() == 1) {
            //remove text from fields
            EditText a = amountOfDiceList.get(index);
            EditText s = sideSelectorList.get(index);

            a.setText("");
            s.setText("");
        } else {
            //remove objects from list
            LinearLayout rOption = newDiceOptions.get(index);
            Button rButton = removeButtonsList.get(index);

            removeButtonsList.remove(index);
            newDiceOptions.remove(index);
            amountOfDiceList.remove(index);
            sideSelectorList.remove(index);

            //remove object from layout
            options.removeView(rButton);
            options.removeView(rOption);

            setUpView();
        }
    }

    public void setUpView() {

        if(newDiceOptions.size() == 1){
            RelativeLayout.LayoutParams BLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            BLP.addRule(RelativeLayout.BELOW, newDiceOptions.get(0).getId());
            addDice.setLayoutParams(BLP);
            return;
        }

        for (int i = 1; i <= newDiceOptions.size() - 1; i++) {
            LinearLayout optionRow = newDiceOptions.get(i);
            Button removeButton = removeButtonsList.get(i);

            options.removeView(optionRow);
            options.removeView(removeButton);

            //add new options to options table
            RelativeLayout.LayoutParams RLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            RLP.addRule(RelativeLayout.BELOW, newDiceOptions.get(i - 1).getId());
            optionRow.setLayoutParams(RLP);

            //move add new dice button to end
            RelativeLayout.LayoutParams BLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            BLP.addRule(RelativeLayout.BELOW, newDiceOptions.get(i).getId());
            addDice.setLayoutParams(BLP);

            //add new remove button
            RelativeLayout.LayoutParams removeLP = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            removeLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            removeLP.addRule(RelativeLayout.BELOW, newDiceOptions.get(i - 1).getId());
            removeLP.height = 100;
            removeLP.width = 100;
            removeButton.setLayoutParams(removeLP);

            options.removeView(addDice);
            options.addView(addDice);
            options.addView(removeButton);

            options.addView(optionRow);
        }
    }

    public void addNewDice() {
        LinearLayout newOptions = new LinearLayout(this); //create options container
        EditText newAmount = new EditText(this);
        TextView newD = new TextView(this);
        EditText newSide = new EditText(this);
        Button removeDice = new Button(this);

        //amount formatting
        newAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
        newAmount.setHint("Amount");
        newAmount.setGravity(Gravity.CENTER);

        newD.setText("d");
        newD.setGravity(Gravity.CENTER);
        newD.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        newD.setTypeface(null, Typeface.BOLD);

        //sides formatting
        newSide.setInputType(InputType.TYPE_CLASS_NUMBER);
        newSide.setHint("Sides");
        newSide.setGravity(Gravity.CENTER);

        //formatting options box
        newOptions.setOrientation(LinearLayout.HORIZONTAL);
        newOptions.setGravity(Gravity.CENTER);
        newOptions.setId(currentID);

        currentID++;

        //add option fields
        newOptions.addView(newAmount);
        newOptions.addView(newD);
        newOptions.addView(newSide);

        //add remove dice buttons
        removeDice.setText("X");
        removeDice.setTextColor(Color.WHITE);
        removeDice.setGravity(Gravity.CENTER);
        removeDice.setPadding(2, 2, 2, 2);
        removeDice.setBackgroundResource(R.drawable.round_button_rem);
        removeDice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
        removeDice.setId(removeID);

        removeID++;

        removeDice.setOnClickListener(this);

        //add objects to the correct lists
        newDiceOptions.add(newOptions);
        amountOfDiceList.add(newAmount);
        sideSelectorList.add(newSide);
        removeButtonsList.add(removeDice);

        setUpView();
    }

    public void gotoMM(View view) {
        Intent goToMD = new Intent(this, MainActivity.class);
        startActivity(goToMD);
    }
}
