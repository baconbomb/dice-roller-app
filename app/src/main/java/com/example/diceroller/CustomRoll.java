package com.example.diceroller;

import java.io.Serializable;

public class CustomRoll implements Serializable {

    DiceInfo dice[];

    CustomRoll(int numberOfCustomDice){
        dice = new DiceInfo[numberOfCustomDice];
        for(int i = 0; i < numberOfCustomDice; i++){
            dice[i] = new DiceInfo(0, 0);
        }
    }
}
