package com.example.diceroller;

import java.io.Serializable;

public class DiceInfo implements Serializable {

    private int numberOfDice;
    private int numberOfSide;

    public DiceInfo(int numberOfDice, int numberOfSide) {
        super();
        this.numberOfDice = numberOfDice;
        this.numberOfSide = numberOfSide;
    }

    public int getNumberOfDice(){
        return numberOfDice;
    }

    public int getNumberOfSide(){
        return numberOfSide;
    }

    public void setNumberOfSide(int sides){
        this.numberOfSide = sides;
    }

    public void setNumberOfDice(int number){
        this.numberOfDice = number;
    }
}
