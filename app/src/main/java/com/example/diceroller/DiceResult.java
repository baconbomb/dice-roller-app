package com.example.diceroller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

public class DiceResult extends Activity {

    private Random rNum = new Random(System.currentTimeMillis());
    private LinearLayout table;
    private TextView diceDescritption, SumText;
    private int number;
    DiceInfo DI;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dice_result);

        Intent getDiceInformation = getIntent();

        DI = (DiceInfo) getDiceInformation.getSerializableExtra("Dice Information");

        SumText = findViewById(R.id.SumOfDice);
        diceDescritption = findViewById(R.id.dice_results_description);
        table = findViewById(R.id.results_table);

        number = DI.getNumberOfDice();

        rollDice();
    }



    public void rollDice(){
        SumText.setText("Total = ");

        int sides = DI.getNumberOfSide();

        diceDescritption.setText(number + "d" + sides);

        int sum = 0;

        for(int i = 0; i < number ; i++){

            int diceN = i + 1;

            int randomN = rNum.nextInt(sides) + 1;
            sum += randomN;

            RelativeLayout dice = new RelativeLayout(this);
            ImageView diceImage = new ImageView(this);
            TextView diceAmount = new TextView(this);
            TextView diceNumber = new TextView(this);
            LinearLayout tableRow = new LinearLayout(this);

            //set dice image params
            diceImage.setImageResource(getDiceImage(sides, randomN));
            diceImage.setLayoutParams(new LinearLayout.LayoutParams(150, 150));

            dice.addView(diceImage);
            dice.addView(diceAmount);

            //set dice roll number params
            RelativeLayout.LayoutParams layoutParams =
                    (RelativeLayout.LayoutParams)diceAmount.getLayoutParams();
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            diceAmount.setLayoutParams(layoutParams);
            diceAmount.setText("" + randomN);


            //set number of dice
            diceNumber.setText("Dice #" + diceN + " d" + sides);
            diceNumber.setPadding(0,  0, 100, 0);

            //create table row
            tableRow.addView(diceNumber);
            tableRow.addView(dice);

            //set table row params
            LinearLayout.LayoutParams tableRowParams =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
            tableRow.setGravity(Gravity.CENTER);
            tableRowParams.setMargins(0,20,20,0);
            tableRow.setLayoutParams(tableRowParams);

            //add table row to table
            table.addView(tableRow);
        }

        SumText.setText("Total = " + sum);
    }

    private int getDiceImage(int sides, int roll) {

        switch (sides){
            case 4:
                return R.drawable.d4;
            case 6:
                return R.drawable.d6;
            case 8:
                return R.drawable.d8;
            case 10:
                return R.drawable.d10;
            case 12:
                return R.drawable.d12;
            case 20:
                return R.drawable.d20;
            default:
                return R.drawable.d20;
        }
    }

    public void reRollDice(View view) {
        table.removeAllViews();
        rollDice();
    }

    public void getNewDice(View view) {
        Intent goToMultiDice = new Intent(this, MultiDice.class);

        startActivity(goToMultiDice);
    }
}
