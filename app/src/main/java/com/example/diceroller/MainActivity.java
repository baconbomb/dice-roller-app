package com.example.diceroller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView imageViewDice;
    private Random rNum = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sixSidedDice(View view) {

        Intent goToSixSideDice = new Intent(this, SixSideDice.class);

        startActivity(goToSixSideDice);
    }

    public void multiDice(View view) {

        Intent goToMultiDice = new Intent(this, MultiDice.class);

        startActivity(goToMultiDice);
    }
}
