package com.example.diceroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MultiDice extends Activity {

    private Spinner diceTypeSpinner;
    private int dNum;
    private int diceAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.multi_dice);

        addItemsToTypeSpinner();

        addListenerToTypeSpinner();
    }

    private void addItemsToTypeSpinner() {

        diceTypeSpinner = findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> diceTypeSpinnerAdapter =
                ArrayAdapter.createFromResource(this,
                        R.array.dice_types, android.R.layout.simple_spinner_item);

        diceTypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        diceTypeSpinner.setAdapter(diceTypeSpinnerAdapter);

    }

    private void addListenerToTypeSpinner() {
        diceTypeSpinner = findViewById(R.id.spinner);

        diceTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String typeSelectedInSpinner = adapterView.getItemAtPosition(i).toString();
                if(typeSelectedInSpinner.equals("d4")){
                    dNum = 4;
                } else if(typeSelectedInSpinner.equals("d6")){
                    dNum = 6;
                } else if(typeSelectedInSpinner.equals("d8")){
                    dNum = 8;
                } else if(typeSelectedInSpinner.equals("d10")){
                    dNum = 10;
                } else if(typeSelectedInSpinner.equals("d12")){
                    dNum = 12;
                } else if(typeSelectedInSpinner.equals("d20")){
                    dNum = 20;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void rollDice(View view) {

        TextView NumberOfDice = findViewById(R.id.DiceNumber);

        if(TextUtils.isEmpty(NumberOfDice.getText()) ) {
            Toast t = Toast.makeText(this, "Please enter number of dice", Toast.LENGTH_SHORT);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            return;
        }

        diceAmount = Integer.valueOf(NumberOfDice.getText().toString());

        DiceInfo DI = new DiceInfo(diceAmount, dNum);

        Intent goToResult = new Intent(this, DiceResult.class);

        goToResult.putExtra("Dice Information", DI);

        startActivity(goToResult);
    }

    public void advancedOptions(View view) {
        Intent goToAdvancedOptions = new Intent(this, AdvancedOptions.class);

        startActivity(goToAdvancedOptions);
    }

    public void goToMainMenu(View view) {
        Intent goToMainMenu = new Intent(this, MainActivity.class);

        startActivity(goToMainMenu);
    }
}
