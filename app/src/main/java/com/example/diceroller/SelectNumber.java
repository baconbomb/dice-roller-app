package com.example.diceroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SelectNumber extends Activity {

    private int numberOfTypes;
    private LinearLayout numberSelector;
    private EditText[] numberInput;
    private CustomRoll customRoll;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_number);

        Intent getCustomRoll = getIntent();

        customRoll = (CustomRoll) getCustomRoll.getSerializableExtra("Custom roll");

        numberSelector = findViewById(R.id.number_selector);

        numberOfTypes = customRoll.dice.length;

        createOptionFields();
    }

    private void createOptionFields() {

        numberInput = new EditText[numberOfTypes];

        for(int i = 0; i < numberOfTypes; i++){

            //set up input container params
            LinearLayout inputContainer = new LinearLayout(this);
            LinearLayout.LayoutParams iCParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            inputContainer.setLayoutParams(iCParams);

            //store dice number
            int diceNumber = i + 1;

            //create new views
            TextView prompt = new TextView(this);
            numberInput[i] = new EditText(this);

            //set up views
            prompt.setText("Number of d" + customRoll.dice[i].getNumberOfSide() + " dice");
            numberInput[i].setInputType(InputType.TYPE_CLASS_NUMBER);
            LinearLayout.LayoutParams sideInputParam =
                    new LinearLayout.LayoutParams
                            (ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            numberInput[i].setLayoutParams(sideInputParam);

            //center views
            numberInput[i].setGravity(Gravity.CENTER);
            prompt.setGravity(Gravity.CENTER);

            //layout params for container
            inputContainer.setOrientation(LinearLayout.VERTICAL);

            //add to container
            inputContainer.addView(prompt);
            inputContainer.addView(numberInput[i]);
            numberSelector.addView(inputContainer);
        }
    }

    public void rollCustomDice(View view) {
        //verify input in all fields
        for(int i = 0; i < numberOfTypes; i++) {
            if (TextUtils.isEmpty(numberInput[i].getText())) {
                Toast t = Toast.makeText(this, "Please enter number of types", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                return;
            }

            customRoll.dice[i].setNumberOfDice
                    (Integer.valueOf(numberInput[i].getText().toString()));
        }
        Intent rollCustomDice = new Intent(this, RollCustomDice.class);
        rollCustomDice.putExtra("Custom roll", customRoll);

        startActivity(rollCustomDice);
    }

    public void goBack(View view) {
        Intent goToSelectTypes = new Intent(this, SelectTypes.class);

        goToSelectTypes.putExtra("Custom roll", customRoll);

        startActivity(goToSelectTypes);

    }
}
