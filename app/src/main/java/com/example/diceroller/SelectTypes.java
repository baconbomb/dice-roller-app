package com.example.diceroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SelectTypes extends Activity {

    private int numberOfTypes;
    private LinearLayout typeSelector;
    private EditText[] sideInput;
    private CustomRoll customRoll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_types);

        Intent getCustomRoll = getIntent();

        customRoll = (CustomRoll) getCustomRoll.getSerializableExtra("Custom roll");

        typeSelector = findViewById(R.id.types_selector);

        numberOfTypes = customRoll.dice.length;

        createOptionFields();
    }


    public void createOptionFields(){

        sideInput = new EditText[numberOfTypes];

        for(int i = 0; i < numberOfTypes; i++){

            //set up input container params
            LinearLayout inputContainer = new LinearLayout(this);
            LinearLayout.LayoutParams iCParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            inputContainer.setLayoutParams(iCParams);

            //store dice number
            int diceNumber = i + 1;

            //create new views
            TextView prompt = new TextView(this);
            sideInput[i] = new EditText(this);

            //set up views
            prompt.setText("Number of sides for dice group #" + diceNumber);
            sideInput[i].setInputType(InputType.TYPE_CLASS_NUMBER);
            LinearLayout.LayoutParams sideInputParam =
                    new LinearLayout.LayoutParams
                            (ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            sideInput[i].setLayoutParams(sideInputParam);

            //center views
            sideInput[i].setGravity(Gravity.CENTER);
            prompt.setGravity(Gravity.CENTER);

            //layout params for container
            inputContainer.setOrientation(LinearLayout.VERTICAL);

            //add to container
            inputContainer.addView(prompt);
            inputContainer.addView(sideInput[i]);
            typeSelector.addView(inputContainer);
        }
    }

    public void gotoAdvancedOption(View view) {
        Intent goToAdvancedOptions = new Intent(this, AdvancedOptions.class);

        startActivity(goToAdvancedOptions);
    }

    public void getSideInput(View view) {
        //verify input in all fields
        for(int i = 0; i < numberOfTypes; i++) {
            if (TextUtils.isEmpty(sideInput[i].getText())) {
                Toast t = Toast.makeText(this, "Please enter number of types", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                return;
            }

            customRoll.dice[i].setNumberOfSide
                    (Integer.valueOf(sideInput[i].getText().toString()));
        }
        Intent selectNumberOfDice = new Intent(this, SelectNumber.class);
        selectNumberOfDice.putExtra("Custom roll", customRoll);

        startActivity(selectNumberOfDice);
    }
}

