package com.example.diceroller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class SixSideDice extends Activity {

    private ImageView imageViewDice;
    private TextView rollTracker;
    private Button reRollBtn, mainMenuBtn;
    private Random rNum = new Random(System.currentTimeMillis());
    private int rolls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.six_side_dice);

        rolls = 0;

        rollTracker = findViewById(R.id.rolltracker);
        imageViewDice = findViewById(R.id.imageViewDice);
        reRollBtn = findViewById(R.id.SixReRoll);
        mainMenuBtn = findViewById(R.id.MM);

        int randomNumber = rNum.nextInt(6) + 1;

        switch (randomNumber) {
            case 1:
                imageViewDice.setImageResource(R.drawable.dice1);
                break;
            case 2:
                imageViewDice.setImageResource(R.drawable.dice2);
                break;
            case 3:
                imageViewDice.setImageResource(R.drawable.dice3);
                break;
            case 4:
                imageViewDice.setImageResource(R.drawable.dice4);
                break;
            case 5:
                imageViewDice.setImageResource(R.drawable.dice5);
                break;
            case 6:
                imageViewDice.setImageResource(R.drawable.dice6);
                break;
        }

        mainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToMainMenu();
            }
        });

        reRollBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rollDice();
            }
        });

        imageViewDice.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            rollDice();
        }
    });
}

    private void rollDice(){

        int randomNumber = rNum.nextInt(6) + 1;
        rolls++;
        rollTracker.setText("Rolls = " + rolls);


        switch (randomNumber){
            case 1:
                imageViewDice.setImageResource(R.drawable.dice1);
                break;
            case 2:
                imageViewDice.setImageResource(R.drawable.dice2);
                break;
            case 3:
                imageViewDice.setImageResource(R.drawable.dice3);
                break;
            case 4:
                imageViewDice.setImageResource(R.drawable.dice4);
                break;
            case 5:
                imageViewDice.setImageResource(R.drawable.dice5);
                break;
            case 6:
                imageViewDice.setImageResource(R.drawable.dice6);
                break;
        }
    }

    public void goToMainMenu() {
        Intent goToMainMenu = new Intent(this, MainActivity.class);

        startActivity(goToMainMenu);
    }

}
